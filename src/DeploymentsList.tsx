import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Loader, Table } from "semantic-ui-react";

import { removeDeploymentThunk } from "./reducks/deployments";
import { State } from "./reducks/reducks";

import "./deploymentList.css";

export const DeploymentsList = () => {
  const dispatch = useDispatch();
  const { list, fetchLoading, removeLoadingList } = useSelector(
    (state: State) => state.deployments
  );

  return (
    <div className="deployment-list">
      <Loader inverted active={fetchLoading} />
      {list.length > 0 && (
        <Table celled inverted selectable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Version</Table.HeaderCell>
              <Table.HeaderCell>Url</Table.HeaderCell>
              <Table.HeaderCell>Deployed at</Table.HeaderCell>
              <Table.HeaderCell />
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {list.map((d) => (
              <Table.Row key={d._id}>
                <Table.Cell>{d.templateName}</Table.Cell>
                <Table.Cell>{d.version}</Table.Cell>
                <Table.Cell>{d.url}</Table.Cell>
                <Table.Cell>{d.deployedAt}</Table.Cell>
                <Table.Cell>
                  <Button
                    disabled={removeLoadingList.includes(d._id)}
                    loading={removeLoadingList.includes(d._id)}
                    onClick={() => dispatch(removeDeploymentThunk(d._id))}
                  >
                    Delete
                  </Button>
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      )}
    </div>
  );
};
