import {
  Deployment,
  DeploymentRequestTO,
  DeploymentsResponseTO,
} from "../types";
import { Action, Dispatch } from "redux";
import axios from "axios";
import { apiBasePath } from "../constants";

interface ActionWithPayload<P = any> extends Action {
  payload: P;
}

type FetchDeploymentsSuccessAction = ActionWithPayload<DeploymentsResponseTO>;
type AddDeploymentSuccessAction = ActionWithPayload<Deployment>;
type RemoveDeploymentAction = ActionWithPayload<string>;

const types = {
  FETCH_DEPLOYMENTS: "FETCH_DEPLOYMENTS",
  FETCH_DEPLOYMENTS_SUCCESS: "FETCH_DEPLOYMENTS_SUCCESS",
  FETCH_DEPLOYMENTS_FAILURE: "FETCH_DEPLOYMENTS_FAILURE",
  ADD_DEPLOYMENT: "ADD_DEPLOYMENT",
  ADD_DEPLOYMENT_SUCCESS: "ADD_DEPLOYMENT_SUCCESS",
  ADD_DEPLOYMENT_FAILURE: "ADD_DEPLOYMENT_FAILURE",
  REMOVE_DEPLOYMENT: "REMOVE_DEPLOYMENT",
  REMOVE_DEPLOYMENT_SUCCESS: "REMOVE_DEPLOYMENT_SUCCESS",
  REMOVE_DEPLOYMENT_FAILURE: "REMOVE_DEPLOYMENT_FAILURE",
};

//actions
export const fetchDeploymentsThunk = () => {
  return function (dispatch: Dispatch) {
    dispatch({
      type: types.FETCH_DEPLOYMENTS,
    });

    return axios
      .get<DeploymentsResponseTO>(`${apiBasePath}/api/deployments`)
      .then((response) => {
        dispatch({
          type: types.FETCH_DEPLOYMENTS_SUCCESS,
          payload: response.data,
        });
      })
      .catch(() => {
        dispatch({
          type: types.FETCH_DEPLOYMENTS_FAILURE,
        });
      });
  };
};

export const addDeploymentThunk = (data: DeploymentRequestTO) => {
  return function (dispatch: Dispatch) {
    dispatch({
      type: types.ADD_DEPLOYMENT,
    });

    return axios
      .post<Deployment>(`${apiBasePath}/api/deployments`, data)
      .then((response) => {
        dispatch({
          type: types.ADD_DEPLOYMENT_SUCCESS,
          payload: response.data,
        });
      })
      .catch(() => {
        dispatch({
          type: types.ADD_DEPLOYMENT_FAILURE,
        });
      });
  };
};

export const removeDeploymentThunk = (id: string) => {
  return function (dispatch: Dispatch) {
    dispatch({
      type: types.REMOVE_DEPLOYMENT,
      payload: id,
    });

    return axios
      .delete(`${apiBasePath}/api/deployments/${id}`)
      .then((response) => {
        dispatch({
          type: types.REMOVE_DEPLOYMENT_SUCCESS,
          payload: id,
        });
      })
      .catch(() => {
        dispatch({
          type: types.ADD_DEPLOYMENT_FAILURE,
          payload: id,
        });
      });
  };
};

// reducer & state
export interface DeploymentsReducerState {
  addLoading: boolean;
  fetchLoading: boolean;
  removeLoadingList: string[];
  list: DeploymentsResponseTO;
}

const DEPLOYMENTS_REDUCER_DEFAULT_STATE: DeploymentsReducerState = {
  addLoading: false,
  fetchLoading: false,
  removeLoadingList: [],
  list: [],
};

export const deploymentsReducer = (
  state: DeploymentsReducerState = DEPLOYMENTS_REDUCER_DEFAULT_STATE,
  action:
    | FetchDeploymentsSuccessAction
    | AddDeploymentSuccessAction
    | RemoveDeploymentAction
    | Action
): DeploymentsReducerState => {
  switch (action.type) {
    case types.FETCH_DEPLOYMENTS: {
      return {
        ...state,
        fetchLoading: true,
      };
    }
    case types.FETCH_DEPLOYMENTS_SUCCESS: {
      return {
        ...state,
        fetchLoading: false,
        list: (action as FetchDeploymentsSuccessAction).payload,
      };
    }
    case types.FETCH_DEPLOYMENTS_FAILURE: {
      return {
        ...state,
        fetchLoading: false,
      };
    }
    case types.ADD_DEPLOYMENT: {
      return {
        ...state,
        addLoading: true,
      };
    }
    case types.ADD_DEPLOYMENT_SUCCESS: {
      return {
        ...state,
        addLoading: false,
        list: [(action as AddDeploymentSuccessAction).payload, ...state.list],
      };
    }
    case types.ADD_DEPLOYMENT_FAILURE: {
      return {
        ...state,
        addLoading: false,
      };
    }
    case types.REMOVE_DEPLOYMENT: {
      return {
        ...state,
        removeLoadingList: [
          ...new Set(state.removeLoadingList),
          (action as RemoveDeploymentAction).payload,
        ],
      };
    }
    case types.REMOVE_DEPLOYMENT_SUCCESS: {
      return {
        ...state,
        removeLoadingList: state.removeLoadingList.filter(
          (el) => el !== (action as RemoveDeploymentAction).payload
        ),
        list: state.list.filter(el => el._id !== (action as RemoveDeploymentAction).payload)
      };
    }
    case types.REMOVE_DEPLOYMENT_FAILURE: {
      return {
        ...state,
        removeLoadingList: state.removeLoadingList.filter(
          (el) => el !== (action as RemoveDeploymentAction).payload
        ),
      };
    }
    default: {
      return state;
    }
  }
};
