import { combineReducers } from "redux";
import { deploymentsReducer, DeploymentsReducerState } from "./deployments";

export interface State {
  deployments: DeploymentsReducerState;
}

export const rootReducer = combineReducers<State>({
  deployments: deploymentsReducer,
});
