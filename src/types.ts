export interface Deployment {
  _id: string;
  templateName: string;
  version: string;
  url?: string;
  deployedAt?: string;
}

export type DeploymentsResponseTO = Deployment[];

export type DeploymentRequestTO = {
  url: string;
  templateName: string;
  version: string;
}