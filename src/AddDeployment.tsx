import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Input } from "semantic-ui-react";
import { Field, FieldProps, Form, Formik } from "formik";

import { DeploymentRequestTO } from "./types";
import { addDeploymentThunk } from "./reducks/deployments";
import { State } from "./reducks/reducks";

import "./addDeployment.css";

const initialValues: DeploymentRequestTO = {
  url: "",
  templateName: "",
  version: "",
};

export const AddDeployment = () => {
  const dispatch = useDispatch();
  const { addLoading } = useSelector((state: State) => state.deployments);

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        dispatch(addDeploymentThunk(values));
      }}
    >
      {() => (
        <Form className="add-deployment__form">
          <Field name="templateName">
            {({ field }: FieldProps) => (
              <Input
                {...field}
                inverted
                label="name"
                placeholder="type deployment name"
              />
            )}
          </Field>
          <Field name="version">
            {({ field }: FieldProps) => (
              <Input
                {...field}
                inverted
                label="version"
                placeholder="type deployment version"
              />
            )}
          </Field>
          <Field name="url">
            {({ field }: FieldProps) => (
              <Input
                {...field}
                inverted
                label="url"
                placeholder="type deployment url"
              />
            )}
          </Field>
          <div>
            <Button
              inverted
              disabled={addLoading}
              loading={addLoading}
              type="submit"
            >
              Submit
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  );
};
