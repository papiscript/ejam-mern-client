The app is available under url: https://ejam-mern.herokuapp.com

Be aware it's heroku free hosting, where after 30 mins of inactivity the app is stopped. 
Then you'll have to wait several seconds until it will start up again.

## What's that?

This is the client - frontend part of eJam-mern interview app. 

The app is a typical POC, so one won't find here extensive (or any) tests, validation, error handling etc.
Some of those `features` are included in TODO section. It's not totally impossible they won't be done eventually :)

## Local development

#### Configuration
Set proper address to ejam-mern-api in `package.json` > `proxy`. Default value is fine when you run the api on specified port.

#### Install & run
1. Run `yarn` or `npm i` to install dependencies
2. Run `yarn start` or `npm run start` to start the app

## TODO:
- bitbucket-heroku integration
- validation FE and BE (yup, separate validation systems)
- errorHandling FE and BE
- logging
- Some basic cypress tests
- pagination
- sorting
- filtering
- delete confirmation modal
- get rid of the warning (so technically it's contributing to semantic or replacing component - meh)
- unify types fe-be