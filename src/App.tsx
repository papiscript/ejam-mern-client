import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import { fetchDeploymentsThunk } from "./reducks/deployments";
import { AddDeployment } from "./AddDeployment";
import { DeploymentsList } from "./DeploymentsList";

import "./App.css";

export const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchDeploymentsThunk());
  }, [dispatch]);

  return (
    <div className="App">
      <AddDeployment />
      <DeploymentsList />
    </div>
  );
};
